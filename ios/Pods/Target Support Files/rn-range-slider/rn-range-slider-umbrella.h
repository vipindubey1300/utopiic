#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "RangeSlider.h"

FOUNDATION_EXPORT double rn_range_sliderVersionNumber;
FOUNDATION_EXPORT const unsigned char rn_range_sliderVersionString[];

