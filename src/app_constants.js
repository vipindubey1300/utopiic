export const colors = {
  STATUS_BAR_COLOR: "#000000",
  COLOR_PRIMARY:'#BB9A5C',
  COLOR_SECONDARY:'#000000',
  WHITE:'#FFFFFF',
  BLACK:'#000000',
  LIGHT_MAROON:'#301606',
  DARK_MAROON:'#170B03',
  LIGHT_YELLOW:'#FAFAD2',
  SILVER:' #C0C0C0',
  LIGHT_DARK:'#1e1e1e',
  LIGHT_GREEN:'#7CFC00',
  CRIMSON:'#DC143C',
  GREEN:'#008000',
  MAROON:'#2f0909',
  PALE_GREEN:'#98FB98',
  GREY:'#D3D3D3',
  DARK_GREY:'#C0C0C0',
  RED: "#ec0008",
  PAGE_BACKGROUND:'#F6F4FC',
  LIGHT_BLACK:'#1E1E1E',
  HEADING_COLOR:'#bb9a5c'
};

import {Dimensions} from 'react-native';


export const urls = {
  base_url: 'http://admin.utopiic.com/rest/',
  base_url_media: 'http://admin.utopiic.com/',
  
};

export const fonts = {
  heading_font: "Helvetica",
};


export const dimensions = {
  SCREEN_WIDTH : Dimensions.get('window').width,
  SCREEN_HEIGHT : Dimensions.get('window').height
};



export const values = {
  STRIPE_DEV_KEY :'pk_test_lH3nLCADTo6rIyxvns0P8igB008LHXo6qB',
 
};
