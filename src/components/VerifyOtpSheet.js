import React from 'react';
import { StyleSheet, View, TextInput, Text,Image, Platform,TouchableOpacity,Animated} from 'react-native';

import {colors,urls,dimensions} from '../app_constants';
import FastImage from 'react-native-fast-image'
import RBSheet from "react-native-raw-bottom-sheet";

import LabelButtonInput from './LabelButtonInput';

export default class VerifyOtpSheet extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          loading_status:false,
        
      };
    }

    _verifyOtp = () =>{
     this.props.success()
      // var formData = new FormData();
      // formData.append('preference_id',this.state.preferences[this.state.selectedIndex].id);
      //console.log('responseJson',formData)
      this.setState({loading_status:true})
    
       let url = urls.base_url +'users/confirmation_otp'
          
            fetch(url, {
                    method: 'POST',
                   // body:formData,
                    headers: {
                      //'Content-Type': 'application/x-www-form-urlencoded',
                     // 'token': this.props.user.token,
                    }
    
                    }).then((response) => response.json())
                        .then((responseJson) => {

                         console.log("))))))))))",responseJson)
                          this.setState({loading_status:false})

                          if(responseJson.status){
                          // this.props.navigation.navigate("Home")
                          }
                          else{
                            ToastAndroid.show(responseJson.message,ToastAndroid.SHORT)
                          }
                          
                         
                      }
                        ).catch((error) => {
                          this.setState({loading_status:false})

                         //console.log(error)
                        });
    
    
    
    }
 
    render() {

       
        return(
            <RBSheet
            ref={this.props.inputRef}
            height={180}
            duration={240}
            customStyles={{
              container: {
                alignItems: "center",
                justifyContent:'center',
                backgroundColor:colors.LIGHT_BLACK,
                padding:10,
                borderTopLeftRadius:15,
                 borderTopRightRadius:15,
             
              }
            }}
            animationType='fade'
            minClosingHeight={10}
          >

          <Text style={{color:'grey',marginVertical:10,alignSelf:'flex-start'}}>    An OTP was sent to entered Phone Number</Text>
         <LabelButtonInput
                  placeholder={'Enter OTP'}
                  onSubmitEditing={()=> this._verifyOtp()}
                  onFinish={this._verifyOtp}
                  inputRef={ref => this.otp = ref}
                  ref={ref => this.otpInput = ref}
                  secureTextEntry={true}
                  maxLength={6}
                  autoCapitalize="characters"
                  keyboardType="numeric"
                  textContentType='telephoneNumber'
                  style={{width:'90%'}}
                  returnKeyType="go"
                  buttonLabel={'verify'}/>
            
          </RBSheet>
          )
     
  
  
    }
}





let styles = StyleSheet.create({
  
  overlayStyle: {
      borderRadius:20
     
},
dropDownParent:{flex:1,
    margin:3,
    borderRadius:20,borderWidth:0.7,
    borderColor:'white',
    padding:5
  }

}
)