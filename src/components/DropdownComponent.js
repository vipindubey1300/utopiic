import React from 'react';
import { StyleSheet, View, TextInput, Text,Image, Platform,Dimensions} from 'react-native';

import {colors,urls,dimensions} from '../app_constants';
import RNPickerSelect from 'react-native-picker-select';

const defaultPlaceholder = {
    label: 'Select',
    value: null,
    color: colors.COLOR_PRIMARY,
    
  };



export default class DropdownComponent extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        data: this.props.data ? this.props.data : [],
        selectedValue:null,
        selectedIndex:null,
     
      };
    
  
  }
    


    getSelectedValue = () => ({
      selected_value:this.state.selectedValue,
      selected_index : this.state.selectedIndex
  
    })
  
    render() {
      const { selectedIndex, selectedValue } = this.state;
     
  
      return (
  
        <View style={[styles.container, this.props.style]}>
          

                    <RNPickerSelect
                    //useNativeAndroidPickerStyle={false}
                    placeholder={this.props.placeholder ? this.props.placeholder : defaultPlaceholder}
                    placeholderTextColor={colors.COLOR_PRIMARY}
                    value={this.state.selectedValue}
                    onValueChange={(value,index) => {
                    this.props.onSelectValue ?  this.props.onSelectValue(value) : null
                        this.setState({
                        selectedValue: value,
                        selectedIndex:index,
                        },
                        ()=>{
                            //selectedvalue will return value in data 
                            //selected index is the index of selected data
                            console.log('useNativeAndroidPickerStyle.---------',this.state.selectedValue)
                        })
                    }}
                    items={this.props.data}
                    style={{
                        ...pickerSelectStyles,

                    }} />
                            
         
        </View>
       
      );
  
  
  
    }
  }
  
  DropdownComponent.defaultProps = {
  
    style: {},
    placeholder: defaultPlaceholder,
  

  };
  

const styles = StyleSheet.create({
    container: {
        flexDirection:'row',
        borderColor:colors.COLOR_PRIMARY,
        borderWidth:0.7,
        borderRadius:30,
        backgroundColor:colors.BLACK,
        margin:0,
        overflow:'hidden',
        paddingLeft:14,
        paddingRight:0,
        marginHorizontal:0,
        marginVertical:10
     
       },
  inputText: {
    textAlign:'left',
    fontSize: 16,
    flex:8.5,
    color:colors.WHITE,
    marginHorizontal:3,
    height:50
  },
  imageStyle:{
      flex:1.8,
      height:'100%',
      backgroundColor:colors.COLOR_PRIMARY,
      borderRadius:22
    }
});
const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
      backgroundColor:"transparent",
      fontSize: 16,
      width:Dimensions.get('window').width * 0.8,
      color: 'white',
      alignItems:"center",
      height:50,
     // textAlign:"center",
      justifyContent:"center",
      alignSelf:"center",
      alignItems:"center",
      flex:8.5,
      marginRight:30
    
     
    },
    inputAndroid: {
      height:50,
      fontSize: 16,
      width: dimensions.SCREEN_WIDTH * 0.7,
      color: 'white',
      alignItems:"center",
      textAlign:"center",
      justifyContent:"center",
      alignSelf:"center",
      fontSize:14,
      flex:8.5,
   
     
     
    },
  });
