import React from 'react';
import { StyleSheet, View, TextInput, Text,Image, Platform,TouchableOpacity,Animated} from 'react-native';

import {colors,urls,dimensions} from '../app_constants';
import FastImage from 'react-native-fast-image'
import RBSheet from "react-native-raw-bottom-sheet";
import Carousel from 'react-native-snap-carousel';

import LabelButtonInput from './LabelButtonInput';

export default class BottomSheetGallery extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          loading_status:false,
        
      };
    }

  
    renderPage = ({item, index}) =>{
        console.log("ITEM---",item)
      return (
        <FastImage 
        source={{uri:item.images}}
        style={{height:'100%',width:'100%',backgroundColor:'grey'}}
        resizeMode={FastImage.resizeMode.cover}/>
      );
    }
    
    render() {

       
        return(
            <RBSheet
            ref={this.props.inputRef}
            height={280}
            duration={240}
            customStyles={{
              container: {
                alignItems: "center",justifyContent:'center',backgroundColor:colors.LIGHT_BLACK,
                padding:10,
                borderTopLeftRadius:15,
                 borderTopRightRadius:15,
             
              }
            }}
            animationType='fade'
            minClosingHeight={10}
          >

         
        <View style={{height:'90%',width:dimensions.SCREEN_WIDTH,alignItems:'center'}} >
                <Carousel
                ref={(c) => { this._carousel = c; }}
                data={this.props.gallery.length > 0 ? this.props.gallery : []}
                renderItem={this.renderPage}
                sliderWidth={dimensions.SCREEN_WIDTH * 0.9}
                itemWidth={dimensions.SCREEN_WIDTH * 0.8}
            />
            </View>
            
          </RBSheet>
          )
     
  
  
    }
}





let styles = StyleSheet.create({
  
  overlayStyle: {
      borderRadius:20
     
},
dropDownParent:{flex:1,
    margin:3,
    borderRadius:20,borderWidth:0.7,
    borderColor:'white',
    padding:5
  }

}
)