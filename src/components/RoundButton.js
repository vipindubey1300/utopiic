import React from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,
  TextInput,PermissionsAndroid,
  RefreshControl,Fragment,Platform
} from 'react-native';
import { withNavigation } from 'react-navigation';
import {colors,urls,dimensions} from '../app_constants';
import FastImage from 'react-native-fast-image'




 const RoundButton = (props) => {

  function _onPress()  {
    props.handler()
  }
  
  return (
    <TouchableOpacity style={[styles.container,props.style]}
    onPress={()=> _onPress() }>
    <FastImage 
    source={require('../assets/arrow_forward_black.png')}
    style={styles.buttonImage} 
    resizeMode={FastImage.resizeMode.cover}/>
       
     </TouchableOpacity>
  )
};
RoundButton.defaultProps = {
    handler: () => {},
    style: {},
   
  };

export default RoundButton

const styles = StyleSheet.create({

  container:{
    justifyContent:'center',
    alignItems:'center',
    backgroundColor:colors.COLOR_PRIMARY,
    paddingHorizontal:15,
    paddingVertical:15,
    borderRadius:30,
    marginTop:20,
    marginBottom:10,
    height:50,
    width: 50,
    
  },
  buttonImage:{
      height:25,
      width: 25,
  }
})
