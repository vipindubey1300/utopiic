import React from 'react';
import { StyleSheet, View, TextInput, Text,Image, Platform,TouchableOpacity,Animated} from 'react-native';

import {colors,urls,dimensions} from '../app_constants';
import FastImage from 'react-native-fast-image'
import { Rating, AirbnbRating } from 'react-native-elements';
import StarRating from 'react-native-star-rating';

export default class ExperienceComponent extends React.Component {
    constructor(props) {
        super(props);
        this.delayValue = 500;
        this.state = {
          animatedValue: new Animated.Value(0),
          data:[]
        }
      }

      componentDidMount = () => {
        Animated.spring(this.state.animatedValue, {
          toValue: 1,
          tension: 20,
          useNativeDriver: true
        }).start();
      }
	

  render() {
      const {object} = this.props
      this.delayValue = this.delayValue + 500;
      const translateX = this.state.animatedValue.interpolate({
        inputRange: [0, 1],
        outputRange: [this.delayValue, 1]
      });
    return (
        <Animated.View  style={{ transform: [{ translateX }]}}>
        <TouchableOpacity style={styles.container}  onPress={()=> this.props.clickHandler(this.props.object)}>
        <View
         style={styles.imageContainer} >
            <FastImage 
            source={{uri:object.cover}}
            style={styles.imageStyle} 
            resizeMode={FastImage.resizeMode.cover}/>
        </View>

        <View  style={styles.bottomContainer} >
        <View style={styles.rowContainer}>
          <View style={{flex:9.5}}>
          <Text style={{color:colors.COLOR_PRIMARY}}>{object.name}</Text>
          <Text style={{color:'white',fontSize:12}}>{object.summary ? object.summary : ''}</Text>
        </View>
                  <View style={{flexDirection:'row',alignItems:'center',marginRight:10,flex:3}}>
                  <Text style={{color:'grey'}}>Price : {object.basic_price}  {object.currency}</Text>
                

                </View>
        </View>
             
              </View>

       
        </TouchableOpacity>
        </Animated.View>
     
    );

  }
}



const styles = StyleSheet.create({
  container: {
   backgroundColor:colors.LIGHT_BLACK,
   margin:10,
   height:dimensions.SCREEN_HEIGHT * 0.35,
   width:dimensions.SCREEN_WIDTH * 0.9,
   

  },
  imageContainer:{
    margin:0,
    overflow:'hidden',
   borderRadius:10,
   borderWidth:0.6,
   borderColor:colors.BLACK,
   flex:8
    
   
  },
  bottomContainer:{
   
  //  height:60,
    backgroundColor:colors.LIGHT_BLACK,
    padding: 3,
    justifyContent:'center'

  },

 
  imageStyle:{
  
   height:'100%',width:'100%'
  },
  
  icon:{
      position: 'absolute',right: 10,
      top:0,height:25,width:25
  },
  rowContainer:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems:'center',
    marginHorizontal:7
  }
});