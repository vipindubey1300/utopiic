import React from 'react';
import {  urls, colors } from '../app_constants';
import { addUser, changeUser } from '../actions/actions';
import AnimatedComponent from './AnimatedComponent';

import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,
  TextInput,PermissionsAndroid,
  RefreshControl,Fragment,Platform
} from 'react-native';



export default class PreferencesComponent extends React.Component{
  constructor(props, context) {
    super(props, context);

    this.state ={
      index:0,
    }
}

componentWillMount(){
  this.setState({index:this.props.index})
}


componentWillReceiveProps(nextProps) {
  // You don't have to do this check first, but it can help prevent an unneeded render
  //console.log("componentWillReceiveProps",nextProps)
  if (nextProps.index !== this.props.index) {
      return nextProps.index
  }
  
}

shouldComponentUpdate(nextProps, nextState){
  return nextProps.index != this.props.index
}



  getAlert = () => {
    console.log('getAlert from Child')
    alert('getAlert from Child......'+ JSON.stringify(this.state.index));
  }

    render(){

    console.log("PPPP",this.props.base_url + this.props.preference.image)

      //const {preferences} = this.props.preferences[this.props.index]
        return(

           <AnimatedComponent index={ this.props.index}>

            <View style={{width: Dimensions.get('window').width* 0.7,
            height:'100%',
            padding:0,elevation:7,
            marginBottom:5,
            marginTop:5,marginLeft:2,marginRight:2,
            shadowOpacity:0.2,alignItems:'center',
            backgroundColor:'#1E1E1E',overflow:'hidden',borderRadius:20,borderColor:'grey',borderWidth:0.4}}>

           

   
            <Image style={{width:'100%',height:'87%',borderRadius:0}} 
            resizeMode='stretch'
            source={{uri:this.props.base_url + this.props.preference.image}}></Image>

            
            <Text style={{
              color:colors.COLOR_PRIMARY,
              fontWeight:'bold',fontSize:16,marginBottom:10,marginTop:5,
            textTransform:'capitalize'}}>{this.props.preference.name}</Text>
            

            
    
                
          </View>
          </AnimatedComponent>
        )
    }
}


