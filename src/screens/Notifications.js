import React, {Component} from 'react';
import {Text, View, Image,Dimensions,
  Alert,ToastAndroid,StatusBar,BackHandler,
  ScrollView,StyleSheet,Platform,
  ImageBackground,TouchableOpacity,ActivityIndicator,TextInput,FlatList,SafeAreaView} from 'react-native';
import { colors ,urls,dimensions} from '../app_constants';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import ButtonComponent from '../components/ButtonComponent';

import AsyncStorage from '@react-native-community/async-storage';
import { withNavigationFocus } from 'react-navigation';
// { Indicator, Pages } from 'react-native-pages';
import FastImage from 'react-native-fast-image'
import DateTimePicker from 'react-native-modal-datetime-picker';

import SwiperFlatList from 'react-native-swiper-flatlist';
import { Rating, AirbnbRating } from 'react-native-elements';
import RazorpayCheckout from 'react-native-razorpay';
import { Switch } from 'react-native-paper';


import { connect } from 'react-redux';

import BannerComponent from '../components/BannerComponent';
import RequestComponent from '../components/RequestComponent';
import stripe from 'tipsi-stripe'
import { showMessage } from '../utils/showMessage';

 const d = Dimensions.get("window")



 class Notifications extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
            isSwitchOn:false
      };

    }






    render() {

        return (
          <SafeAreaView style={{flex:1,backgroundColor:'black'}}>
          <View style={{backgroundColor:colors.LIGHT_BLACK}}>

    


          {/** header */}
          <View style={styles.header}>
          <View style={{flexDirection:'row',alignItems:'center'}}>
          <TouchableOpacity
          onPress={()=> {this.props.navigation.goBack()} }>

          <FastImage 
          source={require('../assets/round_back_black.png')}
          style={{height:40,width:40}} 
          resizeMode={FastImage.resizeMode.contain}/>
           </TouchableOpacity>
           <Text style={{color:colors.COLOR_PRIMARY,fontSize:18,marginHorizontal:10}}>Notifications</Text> 

           </View>

          <View style={{margin:12,marginTop:40}}>
            <Text style={{fontWeight:'100',fontSize:14,marginVertical:5,color:'white',width:'80%'}}>Choose whether to recieve or not the incoming notifications.Putting it on will give you fast updates about our new offers and packages.</Text> 
               
          </View>
          </View>

            {/** header end */}

            <View style={{height:dimensions.SCREEN_HEIGHT ,margin:20}}>

                <View style={styles.rowContainer}>

                    <Text style={styles.labelText}>Notifications</Text>
                    <Switch 
                    color={colors.COLOR_PRIMARY}
                    value={this.state.isSwitchOn} 
                    onValueChange={()=> this.setState({isSwitchOn:!this.state.isSwitchOn})} />
                </View>
                <View style={styles.divider}/>



              </View>
              </View>

            

         </SafeAreaView>




        )
    }
}

const mapStateToProps = state => {
	return {

		user: state.user,
	
	};
};



export default connect(mapStateToProps, null)(Notifications);

let styles = StyleSheet.create({
  header:{
    
       backgroundColor:colors.BLACK,
       borderBottomRightRadius:20,
       height:dimensions.SCREEN_HEIGHT * 0.3
  },
 
  bottomContainer:{
    position:'absolute',
    bottom:0,left:0,right:0,
    height:45,
     backgroundColor:'rgba(30,30,30,0.7)',
   // backgroundColor:'rgba(0,0,0,0.91)',
   borderTopColor:'black',
   borderTopWidth:0.7

  
  },
  blackContainer:{
    width:'95%',
    paddingHorizontal:5,
    paddingVertical:10,
    backgroundColor:colors.BLACK,
    borderRadius:20,
    marginVertical:5,
    alignSelf:'center',
    
  
   
  },
  yellowText:{
    color:colors.COLOR_PRIMARY
  },
  whiteText:{
    color:colors.WHITE
  },
  rowContainer:{
    flexDirection:'row',
 justifyContent:'space-between',
    alignItems:'center',
   
},
buttonContainer:{
  borderRadius:20,
  backgroundColor:colors.BLACK,
  padding:10,
  justifyContent:'center',
  alignItems: 'center',
  borderWidth:0.5,borderColor:colors.COLOR_PRIMARY,
  marginBottom:10,
  width:150,alignSelf:'center'
},
labelText:{
    fontSize:22,
    color:'white',
    fontWeight:'300',
    lineHeight:30
},
divider:{
    height:0.1,width:dimensions.SCREEN_WIDTH * 0.9,
    backgroundColor:'#eee',
    marginVertical:20
}

}
)
