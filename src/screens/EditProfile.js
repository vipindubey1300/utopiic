import React, {Component} from 'react';
import {Text, View, Image,Dimensions,
  Alert,ToastAndroid,StatusBar,BackHandler,
  ScrollView,StyleSheet,Platform,
  ImageBackground,TouchableOpacity,
  ActivityIndicator,TextInput, SafeAreaView,Keyboard,} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { withNavigationFocus } from 'react-navigation';
import Video from 'react-native-video';
import { FlatList } from 'react-native-gesture-handler';
import { Button } from 'native-base';
import FastImage from 'react-native-fast-image'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import DocumentPicker from 'react-native-document-picker';


import { colors ,urls,fonts,dimensions} from '../app_constants';
import {showMessage,showToastMessage} from '../utils/showMessage';
import ProgressBar from '../components/ProgressBar';
import TextInputComponent from '../components/TextInputComponent';
import RoundButton from '../components/RoundButton';
import VerifyOtpSheet from '../components/VerifyOtpSheet';
import DropdownComponent from '../components/DropdownComponent';

import { connect } from 'react-redux';
import { addUser,updateUser } from '../actions/actions';
import ButtonComponent from '../components/ButtonComponent';
const languages =[
    {
        label:'USD',
        value:'USD'
    },
    {
        label:'INR',
        value:'INR'
    },
    

]
class EditProfile extends React.Component {

	constructor(props) {
    super(props);
    this.state={
      loading_status:false,
      deviceToken:'324323243cfffgfdgfd',
      pan:null,
      license:null,
      passport:null,
      data:'',
      panData:'',
      licenseData:'',
      passportData:''
    }
  }


       
  componentDidMount =()=>{
    let name = this.props.user.name
    let email = this.props.user.email
    let mobile = this.props.user.phone

    this.nameInput.setState({text:name})
    this.emailInput.setState({text:email})
    this.mobileInput.setState({text:mobile})
}

  loginHandler =() =>{
   this.props.navigation.navigate("Home")
  }


  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  _isValid(){
    
   const name = this.nameInput.getInputValue();
   if(name.trim().length == 0){
      showMessage('Enter Name')
      return false
    }
    else{
      return true;
    }
  }

  _onPasscode = () =>{
   
    if(this._isValid()){
     
        this.setState({loading_status:true})
        var formData = new FormData();
        const passcode = this.passcodeInput.getInputValue();
      
       formData.append('passcode', passcode);
       formData.append('device_id', this.state.deviceToken);
       Platform.OS =='android'
       ?  formData.append('type',1)
       : formData.append('type',2)

       Keyboard.dismiss()
    
     let url = urls.base_url +'auth/passcode'
             
     fetch(url, {
               method: 'POST',
               body: formData
              }).then((response) => response.json())
                   .then(async (responseJson) => {
                       this.setState({loading_status:false})
    
                    console.log("FFF",JSON.stringify(responseJson))
                    
                    if (responseJson.status){

          
    
                      var  user_id = responseJson.data.customer_id.toString()
                      var name = responseJson.data.name
                      var email = responseJson.data.email
                      var image = responseJson.data.customer_image
                      var token = responseJson.data.token
                      var phone = responseJson.data.phone
    
                      AsyncStorage.multiSet([
                        ["user_id", user_id.toString()],
                        ["email", email.toString()],
                        ["name", name.toString()],
                        ["image", image.toString()],
                        ["token", token.toString()],
                        ["phone", phone.toString()],
                        ]);
  
                       await this.props.add({ 
                          user_id: user_id, 
                          name : name,
                          image : image,
                          email:  email ,
                          phone : phone,
                          token:  token 
                        })

                        showMessage('Correct Passcode')
                        
                       // this.setState({passcode:''})
                        //this.props.navigation.navigate('Register');
                      
                       }else{
                        showMessage(responseJson.message)
                        
                        
                      }
                   }).catch((error) => {
                             this.setState({loading_status:false})
                          
                             showToastMessage(error.message)
                   });
     }
   
  }

  _next =()=>{
    if(this._isValid()){
        Keyboard.dismiss()
        const email = this.emailInput.getInputValue();
        const name = this.nameInput.getInputValue();
        const currency = this.currencyDropDown.getSelectedValue()
    
      
          this.setState({loading_status:true})
          var formData = new FormData();
        
         formData.append('name',name);
         formData.append('currency',currency ? currency.selected_value : '');
         formData.append('pan', this.state.pan);
         formData.append('passport',this.state.passport);
         formData.append('license',this.state.license);
         formData.append('images','');

    
           console.log("FFF",JSON.stringify(formData))
      
      
                 let url = urls.base_url +'users/profile_update'
               
                 fetch(url, {
                 method: 'POST',
                 headers: {
                   'Accept': 'application/json',
                   'Content-Type': 'multipart/form-data',
                   'token':this.props.user.token
                 },
                 body: formData
                }).then((response) => response.json())
                     .then(async (responseJson) => {
                         this.setState({loading_status:false})
      
                         console.log("FFF",JSON.stringify(responseJson))
  
      
                      if (!responseJson.error){
                        this.props.updateUser({  name:name }); 
                        this.props.navigation.pop()
      
                       
                                
                         }else{
    
                             showMessage(responseJson.message)
                          
                           }
                     }).catch((error) => {
                       console.log(error.message)
                               this.setState({loading_status:false})
                               showMessage('Try Again')
      
                     });
        }
  }
  
  
 


    _success =()=>{
      this.verifySheet.close()
      this.props.navigation.navigate("Preferences")
      
  }



  _fetchDetails = async () =>{
   // console.log("*********",this.props.user.token)
  this.setState({loading_status:true})
           let url = urls.base_url +'users/profile'
                fetch(url, {
                method: 'POST',
                headers: {
                   'token': this.props.user.token,
                }

                }).then((response) => response.json())
                    .then((responseJson) => {

                     
                      this.setState({loading_status:false})
                      console.log('countries,',responseJson)

                      if(responseJson.status){
                        // var list = responseJson.data.list
                        // console.log('countries,',list)
                        var currency = responseJson.data.currency
                        var email = responseJson.data.email
                        var name = responseJson.data.first_name
                        var last_name = responseJson.data.last_name
                        var phone = responseJson.data.phone
                        var pan = responseJson.data.pan
                        var passport = responseJson.data.passport
                        var license = responseJson.data.license
                        var profile = responseJson.data.profile


                        this.currencyDropDown.setState({selectedValue:currency})
                        this.nameInput.setState({text:name + ' ' + last_name})
                        this.emailInput.setState({text:email})
                        this.mobileInput.setState({text:phone})

                       this.setState({data:responseJson.data,
                            panData:pan,
                          licenseData:license,
                           passportData:passport})
                      }
                      else{
                        
                      }
                      
                     
                  }
                    ).catch((error) => {
                      this.setState({loading_status:false})

                     
                    });



}









componentWillMount(){

 this._fetchDetails()

}

      
_selectPan = async () =>{
    try {
      const res = await DocumentPicker.pick({
       // type: [DocumentPicker.types.images,DocumentPicker.types.pdf],
       type: [DocumentPicker.types.images]
      });
      if(res && res.uri){
         var photo = {
              uri: res.uri,
              type:res.type,
              name: res.name,
          };
          this.setState({pan:photo})

         
        //   var temp = res.type.split('/')
        //   if(temp.length == 2){
        //     console.log('======',temp[1])
        //   }

        //   RNFS.readFile(res.uri, "base64").then(result =>
        //     {
        //        // console.log('base64====',result)
        //         this.setState({
        //           document:result,document_type:temp.length == 2 ? temp[1] : res.type
        //         })
        //     })


      
            
          
      }
     
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        showMessage('User cancelled the picker')
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        showMessage('Try Again')
      }
    }

}



_selectPassport = async () =>{
    try {
      const res = await DocumentPicker.pick({
       // type: [DocumentPicker.types.images,DocumentPicker.types.pdf],
       type: [DocumentPicker.types.images]
      });
      if(res && res.uri){
         var photo = {
              uri: res.uri,
              type:res.type,
              name: res.name,
          };
          this.setState({passport:photo})     
      }
     
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        showMessage('User cancelled the picker')
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        showMessage('Try Again')
      }
    }

 }

 _selectLicense = async () =>{
    try {
      const res = await DocumentPicker.pick({
       // type: [DocumentPicker.types.images,DocumentPicker.types.pdf],
       type: [DocumentPicker.types.images]
      });
      if(res && res.uri){
         var photo = {
              uri: res.uri,
              type:res.type,
              name: res.name,
          };
          this.setState({license:photo})  
          
      }
     
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        showMessage('User cancelled the picker')
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        showMessage('Try Again')
      }
    }

}



 _checkPan =() =>{

    if(this.state.pan === null && this.state.panData.length == 0){
      return (
        <FastImage 
        source={require('../assets/plus-empty-yellow.png')}
        style={styles.buttonImage} 
        resizeMode={FastImage.resizeMode.cover}/>
          )
    }
    else if(this.state.pan === null && this.state.panData.length > 0){
      return (  <FastImage 
        source={{uri:this.state.panData}}
        style={styles.buttonImage} 
        resizeMode={FastImage.resizeMode.cover}/>)
    }
    else{
        return (  <FastImage 
            source={require('../assets/checkcircle-filled-yellowpng.png')}
            style={styles.buttonImage} 
            resizeMode={FastImage.resizeMode.cover}/>)
    }


  }



 _checkPassport =() =>{

    if(this.state.passport === null && this.state.passportData.length == 0){
      return (
        <FastImage 
        source={require('../assets/plus-empty-yellow.png')}
        style={styles.buttonImage} 
        resizeMode={FastImage.resizeMode.cover}/>
          )
    }
    else if(this.state.passport === null && this.state.passportData.length > 0){
      return (  <FastImage 
        source={{uri:this.state.passportData}}
        style={styles.buttonImage} 
        resizeMode={FastImage.resizeMode.cover}/>)
    }
    else{
        return (  <FastImage 
            source={require('../assets/checkcircle-filled-yellowpng.png')}
            style={styles.buttonImage} 
            resizeMode={FastImage.resizeMode.cover}/>)
    }


  }


  _checkLicense =() =>{

    if(this.state.license === null && this.state.licenseData.length == 0){
      return (
        <FastImage 
        source={require('../assets/plus-empty-yellow.png')}
        style={styles.buttonImage} 
        resizeMode={FastImage.resizeMode.cover}/>
          )
    }
    else if(this.state.license === null && this.state.licenseData.length > 0){
      return (  <FastImage 
        source={{uri:this.state.licenseData}}
        style={styles.buttonImage} 
        resizeMode={FastImage.resizeMode.cover}/>)
    }
    else{
        return (  <FastImage 
            source={require('../assets/checkcircle-filled-yellowpng.png')}
            style={styles.buttonImage} 
            resizeMode={FastImage.resizeMode.cover}/>)
    }


  }


	render() {
    return (
      <>
      <StatusBar barStyle="light-content"  backgroundColor={colors.STATUS_BAR_COLOR} />
      <VerifyOtpSheet
      inputRef={ref => this.verifySheet = ref} //for close/open sheet
      ref={ref => this.verifyOtpSheet = ref}
      success ={this._success}
    />
        <SafeAreaView style={styles.container}>
                  {/** header */}
        <View style={styles.header}>
        <TouchableOpacity style={{flexDirection:'row',alignItems:'center',padding:10}}
        onPress={()=> {this.props.navigation.pop()} }>
          <FastImage 
          source={require('../assets/back.png')}
          style={{height:30,width:30,marginLeft:4,marginRight:10}} 
          resizeMode={FastImage.resizeMode.contain}/>
          <Text style={{color:colors.COLOR_PRIMARY,fontSize:18}}>Edit Profile </Text> 
         </TouchableOpacity>

         <View/>
        </View>

          {/** header end */}
                    <KeyboardAwareScrollView
                    contentContainerStyle={styles.scrollContainer}>

                      <Text style={styles.headingText}>Edit Your Basic Information</Text>
                      <TextInputComponent
                      placeholder={'Enter Name'}
                    //  onSubmitEditing={()=> this._onPasscode()}
                      //onFinish={this._onPasscode}
                      inputRef={ref => this.name = ref}
                      ref={ref => this.nameInput = ref}
                      style={{width:'90%'}}
                      returnKeyType="next"
                  />

                      <TextInputComponent
                        placeholder={'Enter Email'}
                        //onSubmitEditing={()=> this._onPasscode()}
                       // onFinish={this._onPasscode}
                       editable={false}
                        inputRef={ref => this.passcode = ref}
                        ref={ref => this.emailInput = ref}
                        style={{width:'90%',backgroundColor:colors.LIGHT_BLACK}}
                        returnKeyType="next"
                    />

                    <TextInputComponent
                    placeholder={'Enter Phone Number'}
                    //onSubmitEditing={()=> this._onPasscode()}
                    //onFinish={this._onPasscode}
                    keyboardType="numeric"
                    editable={false}
                    textContentType='telephoneNumber'
                    inputRef={ref => this.passcode = ref}
                    ref={ref => this.mobileInput = ref}
                    style={{width:'90%',backgroundColor:colors.LIGHT_BLACK}}
                    returnKeyType="go"
                />

                <DropdownComponent
                data={languages}
                style={{width:'90%'}}
                ref={ref => this.currencyDropDown = ref}
                
                placeholder={{
                  label: 'Currency',
                  value: null,
                  color: colors.COLOR_PRIMARY,
                  
                }}
                />



                {/** documents */}
                {/** was in first version */}
                {/* <View style={{width:'90%',justifyContent:'space-between',alignItems:'center',flexDirection:'row'}}>
                        <TouchableOpacity onPress={()=> this._selectPan()} style={styles.docContainer}>
                            <Text style={{color:'white',margin:10}}>PAN</Text>
                           {this._checkPan()}
                        </TouchableOpacity>

                        <TouchableOpacity onPress={()=> this._selectPassport()} style={styles.docContainer}>
                        <Text style={{color:'white',margin:10}}>Passport</Text>
                        {this._checkPassport()}
                        
                        </TouchableOpacity>

                        <TouchableOpacity onPress={()=> this._selectLicense()} style={styles.docContainer}>
                        <Text style={{color:'white',margin:10}}>Liscence</Text>
                         {this._checkLicense()}
                    </TouchableOpacity>
                </View> */}
                  {/** documents end */}

          <TouchableOpacity onPress={()=> this._selectLicense()} style={[styles.docContainer,{width:dimensions.SCREEN_WIDTH * 0.8}]}>
                        <Text style={{color:'white',margin:10,fontSize:18}}>Govenment ID</Text>
                         {this._checkLicense()}
                    </TouchableOpacity>
                <ButtonComponent
                label={'Save'}
                style={{width:dimensions.SCREEN_WIDTH * 0.6,marginTop:30}}
                handler={this._next}
                />

                    


                
                  
                  </KeyboardAwareScrollView>

                  { this.state.loading_status && <ProgressBar/> }
             
        </SafeAreaView>
      </>
    );
	}
}

const mapStateToProps = state => {
	return {
	
		user: state.user,
	
	};
};

const mapDispatchToProps = dispatch => {
  return {
   
    remove : (user_info) => dispatch(removeUser(user_info)),
    updateUser : (user_info) => dispatch(updateUser(user_info)),
    
    
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(EditProfile);

let styles = StyleSheet.create({
  container:{
  //  height:'100%',
    flex:1,
    backgroundColor:colors.BLACK
  },
  imageContainer:{
      //  position:'absolute',
      //  top:0,left:0,right:0,bottom:0


      //height and width will not go in safe area means notch me nhi 
      //  height:'100%',
      // width:'100%'

      position: 'absolute',
      flex: 1,
      backgroundColor:'rgba(0,0,0,0.45)',
      width:dimensions.SCREEN_WIDTH,
      height: dimensions.SCREEN_HEIGHT
  },

  scrollContainer:{
    padding:10,
    alignItems:'center'
  },
  headerLogo:{
      height: dimensions.SCREEN_HEIGHT * 0.15, 
      width:dimensions.SCREEN_WIDTH * 0.4, 
      margin:10

  },
  headingText:{
    color:colors.WHITE,
    fontSize:18,
    marginVertical:25,
    fontWeight:'bold'
  },
  headingSmallText:{
    color:colors.DARK_GREY,
    fontSize:13,
    marginVertical:15,
    fontWeight:'100'
  },
  docContainer:{
      borderColor:colors.COLOR_PRIMARY,
      borderWidth:0.5,
      borderRadius:10,
      padding:2,alignItems:'center',height: 150,width:90,
      marginVertical:17

  },
  buttonImage:{
      height:80,width:80,margin:4
  }


})

