import React, {Component} from 'react';
import {Text, View, Image,Dimensions,
  Alert,ToastAndroid,StatusBar,BackHandler,
  ScrollView,StyleSheet,Platform,
  ImageBackground,TouchableOpacity,ActivityIndicator,TextInput,FlatList,SafeAreaView} from 'react-native';
import { colors ,urls,dimensions} from '../app_constants';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import ButtonComponent from '../components/ButtonComponent';
import {showMessage,showToastMessage} from '../utils/showMessage';

import AsyncStorage from '@react-native-community/async-storage';
import { withNavigationFocus } from 'react-navigation';
// { Indicator, Pages } from 'react-native-pages';
import FastImage from 'react-native-fast-image'
import DateTimePicker from 'react-native-modal-datetime-picker';

import SwiperFlatList from 'react-native-swiper-flatlist';


import { connect } from 'react-redux';

import BannerComponent from '../components/BannerComponent';
import RoomComponent from '../components/RoomComponent';
import Message from '../components/Message';

 const d = Dimensions.get("window")



 const guest_count=[
   {id:1,value:'1'},
   {id:2,value:'2'},
   {id:3 ,value:'3'}
 ]
 class SelectRoom extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          rooms:[],
          hotel:'',
          from_date:'',
          to_date:'',
          selectedRoom:null,
          selectedGuest:null,
          error:false

      };

    }

   
    _fetchRooms =  (id) =>{
     
      this.setState({loading_status:true})
    
               let url = urls.base_url +'hotels/list_rooms/'+id
               console.log("*---------->>>>",url)
                    fetch(url, {
                    method: 'POST',
                    headers: {
                       'token': this.props.user.token,
                    }
    
                    }).then((response) => response.json())
                        .then((responseJson) => {
                          console.log("*---------->>>>",JSON.stringify(responseJson))
                         
                          this.setState({loading_status:false})

                          if(responseJson.status){
                            var list = responseJson.data.list
                            console.log('rooms,',list)

                            if(list.length == 0) showMessage('No rooms Found for this hotel.')
                            this.setState({rooms:list.splice(0,10)})
                          }
                          else{
                            
                          }
                          
                         
                      }
                        ).catch((error) => {
                          this.setState({loading_status:false})

                         
                        });
    
    
    
    }




  componentWillMount(){
   // console.log(JSON.stringify(this.props.user))
   var result = this.props.navigation.getParam('result')
   var hotel = result['hotel']
   var from_date = result['from_date']
   var to_date = result['to_date']
   console.log('hotel_id-----',result['hotel_id'])
   console.log('from_date-----',result['from_date'])
   console.log('to_date-----',result['to_date'])


   this.setState({
    hotel,from_date,to_date
   })
    
 
   this._fetchRooms(hotel.id)
   
  }


   
  onHotelClick = (item) =>{
    console.log('onHotelClick--------',JSON.stringify(item))
      var id = item.id
      let obj ={'hotel_id':id}
      this.props.navigation.navigate("HotelDetails",{result:obj})
  
  }

  guestView  = (count) =>{
      
     var a =   Array(parseInt(count)).fill().map(Math.random)
     
        return a.map((item) => {
          return (
            <FastImage 
            source={require('../assets/round_back_black.png')}
            style={{height:20,width:20,margin:3,flexWrap:'nowrap'}} 
            resizeMode={FastImage.resizeMode.contain}/>
          );
      });

     
  }

  _facilities = (facilities) =>{
        
       return facilities.map((item) => {
         return (
          <View style={[styles.rowContainer,{marginHorizontal:3}]}>
            <FastImage 
            source={{uri:item.icon}}
            style={{height:15,width:15,margin:3,flexWrap:'nowrap'}} 
            resizeMode={FastImage.resizeMode.contain}/>
            <Text style={[styles.whiteText,{fontSize:10}]}>{item.name}</Text>
          </View>
           
         );
     });

    
 }

 onPressAction = (item) => {
this.setState({selectedRoom:item})
}


  _renderRoom = ({item, index}) =>{
    return (
      <RoomComponent
      {...this.props}
        object = {item} 
        index ={index}
        onSelectRoom={this.onPressAction}
        selected={(this.state.selectedRoom && this.state.selectedRoom.id) == item.id} />
   )
    }



    _renderGuestCount= ({item, index}) =>{
      const isSelectedGuest = this.state.selectedGuest == null ? false : (this.state.selectedGuest.id === item.id)
      
         
       return(
         <TouchableOpacity onPress={()=>{
           this.setState({
             selectedGuest: item
           });
         }}
         style={{
           borderWidth:isSelectedGuest ? 1.2 : 0.5,
           borderColor:isSelectedGuest ? colors.COLOR_PRIMARY : 'grey',
           backgroundColor:isSelectedGuest ? colors.COLOR_PRIMARY : 'black',
           margin:10,
           //width:dimensions.SCREEN_WIDTH * 0.4,
           alignItems:'center',
           justifyContent:'center',
           borderRadius:20,
        
          paddingHorizontal:33,
          paddingVertical:5
          
         }}>
           <Text style={{textAlign:'center',margin:2,fontSize:13,color:isSelectedGuest ? colors.BLACK :'grey'}}>{item.value}</Text>
         </TouchableOpacity>
       )
    }


    _onDone =()=>{
     if(this.state.selectedRoom ){
      var result =   this.props.navigation.getParam('result')

    

      if(result.is_addons == 1){
        //means go to add ons as it contains addon
        let obj ={
          'hotel':result.hotel,
          'from_date':result.from_date,
          'to_date':result.to_date,
          'room':this.state.selectedRoom
        }
        this.props.navigation.navigate("SelectAddOns",{result:obj})
      }
      else{
        let obj ={
          'hotel':result.hotel,
          'from_date':result.from_date,
          'to_date':result.to_date,
          'room':this.state.selectedRoom,
          'add_on':[]
        }
        this.props.navigation.navigate("Review",{result:obj})
      }
      
     }
     else{
       //showMessage("Select Room First !")
      //  this.setState({error: true})
  
      //    setTimeout(() => {
      //      this.setState({error: false})
         
      //    }, 4000)
     }
    }

    
   
    render() {

        return (
          <SafeAreaView style={{flex:1,backgroundColor:'black'}}>
          <ScrollView style={{backgroundColor:colors.LIGHT_BLACK}}>

    


          {/** header */}
          <View style={styles.header}>
          <View style={{flexDirection:'row',alignItems:'center'}}>
          <TouchableOpacity
          onPress={()=> {this.props.navigation.goBack()} }>
          <FastImage 
          source={require('../assets/round_back_black.png')}
          style={{height:40,width:40}} 
          resizeMode={FastImage.resizeMode.contain}/>
           </TouchableOpacity>
           <Text style={{color:colors.COLOR_PRIMARY,fontSize:18,marginHorizontal:10}}>Select Rooms </Text> 

           </View>

          <View style={{margin:12,marginTop:40}}>
            <Text style={{fontWeight:'100',fontSize:14,marginVertical:5,color:colors.COLOR_PRIMARY,width:'100%'}}>Selected Date : {this.state.from_date} - {this.state.to_date}</Text>
            <Text style={{fontWeight:'100',fontSize:14,marginVertical:5,color:colors.WHITE,width:'100%'}}>As per the selected dates, following rooms are available. All prices shown are inclusive/exclusive of taxes</Text>

          </View>
          </View>

            {/** header end */}


                
           

               {/** rooms */}
               <View>
              
                <FlatList
                    style={{marginVertical:10}}
                    horizontal={false}
                    data={this.state.rooms}
                    showsHorizontalScrollIndicator={false}
                    renderItem={(item,index) => this._renderRoom(item,index)}
                    keyExtractor={item => item.id}
                />

               </View>

              {/** hotels end */}




    

             




            
              
              </ScrollView>

              <View style={styles.bottomContainer}>
              <ButtonComponent 
              style={{marginTop:-15,width:dimensions.SCREEN_WIDTH * 0.4,alignSelf:'center'}}
              handler={this._onDone}
              label ={'Request'}/>
          </View>

          {this.state.error && <Message 
          success={false} message={'Select rooms First'}/>}
         </SafeAreaView>




        )
    }
}

const mapStateToProps = state => {
	return {

		user: state.user,
	
	};
};



export default connect(mapStateToProps, null)(SelectRoom);

let styles = StyleSheet.create({
  header:{
    
       backgroundColor:colors.BLACK,
       borderBottomRightRadius:20,
       height:dimensions.SCREEN_HEIGHT * 0.3
  },
 
  
  rowContainer:{
      flexDirection:'row',
     // justifyContent:'space-between',
      alignItems:'center',
     
  },

  dateHeading:{
      color:colors.COLOR_PRIMARY,
      marginVertical:7
  },
  bottomContainer:{
    position:'absolute',
    bottom:0,left:0,right:0,
    height:45,
     backgroundColor:'rgba(30,30,30,0.7)',
   // backgroundColor:'rgba(0,0,0,0.91)',
   borderTopColor:'black',
   borderTopWidth:0.7

  
  },
  roomContainer:{
    width:'95%',
    paddingHorizontal:5,
    paddingVertical:10,
    backgroundColor:colors.BLACK,
    borderRadius:20,
    marginVertical:5,
    alignSelf:'center',
    borderWidth:1,
    borderColor:'black'
   
  },
  yellowText:{
    color:colors.COLOR_PRIMARY
  },
  whiteText:{
    color:colors.WHITE
  },
  rowContainer:{
    flexDirection:'row',
   // justifyContent:'space-between',
    alignItems:'center',
   
},

}
)
