import React, {Component} from 'react';
import {Text, View, Image,Dimensions,
  Alert,ToastAndroid,StatusBar,BackHandler,
  ScrollView,StyleSheet,Platform,
  ImageBackground,TouchableOpacity,ActivityIndicator,TextInput,FlatList,SafeAreaView} from 'react-native';
import { colors ,urls,dimensions} from '../app_constants';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import ButtonComponent from '../components/ButtonComponent';

import AsyncStorage from '@react-native-community/async-storage';
import { withNavigationFocus } from 'react-navigation';
// { Indicator, Pages } from 'react-native-pages';
import FastImage from 'react-native-fast-image'
import DateTimePicker from 'react-native-modal-datetime-picker';

import SwiperFlatList from 'react-native-swiper-flatlist';
import { Rating, AirbnbRating } from 'react-native-elements';
import RazorpayCheckout from 'react-native-razorpay';


import { connect } from 'react-redux';

import BannerComponent from '../components/BannerComponent';
import RequestComponent from '../components/RequestComponent';
import stripe from 'tipsi-stripe'
import { showMessage } from '../utils/showMessage';
import Chat from './Chat';
import ZendeskChat from "react-native-zendesk-chat";
ZendeskChat.init("jGCc8CzMayUufCUYqYqGcewgZt5u1ILi");
 const d = Dimensions.get("window")



 class Requests extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          requests:[],
      };

    }




    _initialize =()=>{
      ZendeskChat.startChat({
          name: 'abc',
          email: 'abc@gmail.com',
          phone: '47564385',
          tags: ["tag1", "tag2"],
          department: "Your department",
          // The behaviorFlags are optional, and each default to 'true' if omitted
          behaviorFlags: {
              showAgentAvailability: true,
              showChatTranscriptPrompt: true,
              showPreChatForm: true,
              showOfflineForm: true,
          },
          // The preChatFormOptions are optional & each defaults to "optional" if omitted
          preChatFormOptions: {
              //name: !user.full_name ? "required" : "optional",
              name:  "required",
              email: "optional",
              phone: "optional",
              department: "required",
          },
          localizedDismissButtonTitle: "Dismiss",
      });
  }
  


    _fetchRequests =  () =>{
        console.log("*********",this.props.user.token)
        this.setState({loading_status:true})
      
                 let url = urls.base_url +'booking/history'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                         'token': this.props.user.token,
                      }
      
                      }).then((response) => response.json())
                          .then((responseJson) => {
                            console.log("*********",responseJson)
                            this.setState({loading_status:false})
  
                            if(responseJson.status){
                              var list = responseJson.data
                            //  console.log('requests,',list)
                             
                             
                             this.setState({requests:list})
                            }
                            else{
                              
                            }
                            
                           
                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})
  
                           
                          });
      
      
      
      }


      _getStripeKey =  (item,token) =>{
        this.setState({loading_status:true})
        let url = urls.base_url +'booking/stripe_public_key'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                         'token': this.props.user.token,
                      }
                      }).then((response) => response.json())
                          .then((responseJson) => {
                            console.log("*********",responseJson)
                            this.setState({loading_status:false})
  
                            if(responseJson.status){
                                
                             try{
                              stripe.setOptions({
                              publishableKey:responseJson.data.public_key,
                                //secretKey: responseJson.data.public_key,
                                //androidPayMode:'production'
                              })
                             }
                             catch(e){
                               showMessage(e.message)
                             }
                            }
                            else{
                              stripe.setOptions({
                                publishableKey:'',
                                //androidPayMode:'production'
                              })
                            }
                            
                           
                        }
                          ).catch((error) => {
                            // stripe.setOptions({
                            //   publishableKey:'',
                            //   //androidPayMode:'production'
                            // })
                            this.setState({loading_status:false})
  
                            showMessage(error.message)
                          });
      
      
      
      }

    componentWillMount(){
        this._getStripeKey()
        this._fetchRequests()
    }


    convertDate(date) {
        var yyyy = date.getFullYear().toString();
        var mm = (date.getMonth()+1).toString();
        var dd  = date.getDate().toString();
    
        var mmChars = mm.split('');
        var ddChars = dd.split('');
    
        return yyyy + '-' + (mmChars[1]?mm:"0"+mmChars[0]) + '-' + (ddChars[1]?dd:"0"+ddChars[0]);
      }
    

    _razorSuccess =  (item,transactionId) =>{
        this.setState({loading_status:true})

        var formData = new FormData();
      
       formData.append('booking_id', item.id);
       formData.append('booking_code', item.booking_code);
       formData.append('transaction_number', transactionId);
       formData.append('currency', item.currency);
       formData.append('total_amount', parseInt(item.basic_price) + parseInt(item.convenience_fees) + parseInt(item.discount) + parseInt(item.publish_price));
       formData.append('payment_date', this.convertDate(new Date()));


        let url = urls.base_url +'booking/razor_success'
                      fetch(url, {
                      method: 'POST',
                      body:formData,
                      headers: {
                         'token': this.props.user.token,
                      }
                      }).then((response) => response.json())
                          .then((responseJson) => {
                            console.log("*********",responseJson)
                            this.setState({loading_status:false})
  
                            if(responseJson.status){
                                this.props.navigation.pop()
                                showMessage("Payment Done successfully !")
                            }
                            else{
                              
                            }
                            
                           
                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})
  
                           
                          });
      
      
      
      }

      _razorFail =  (item) =>{
        this.setState({loading_status:true})

        var formData = new FormData();
      
       formData.append('booking_id', item.id);
       formData.append('booking_code', item.booking_code);
       formData.append('payment_date', this.convertDate(new Date()));


        let url = urls.base_url +'booking/razor_fail'
                      fetch(url, {
                      method: 'POST',
                      body:formData,
                      headers: {
                         'token': this.props.user.token,
                      }
                      }).then((response) => response.json())
                          .then((responseJson) => {
                            console.log("*********",responseJson)
                            this.setState({loading_status:false})
  
                            if(responseJson.status){
                                this.props.navigation.pop()
                            }
                            else{
                              
                            }
                            
                           
                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})
  
                           
                          });
      
      
      
      }
    _razorPay =(item)=>{
        console.log('Paying for---',item)
        var options = {
            description: 'Utopiic - A travel Concierge',
            image: 'https://i.imgur.com/3g7nmJC.png',
            currency: 'INR',
            key: 'rzp_test_Ihan3MUem7MKj2',
            amount: parseInt(item.basic_price) + parseInt(item.convenience_fees) + parseInt(item.discount) + parseInt(item.publish_price),
            name: 'Utopiic',
            prefill: {
              email:this.props.user.email,
              contact: this.props.user.phone,
              name: this.props.user.name,
            },
            theme: {color: '#000000'}
          }
          RazorpayCheckout.open(options).then((data) => {
            // handle success
            console.log(JSON.stringify(data))
            // alert(`Success: ${JSON.stringify(data)}`);
            this._razorSuccess(item,data.razorpay_payment_id)
          }).catch((error) => {
            // handle failure
            alert(`Error:  ${error.description}`);
            this._razorFail(item)
          });
    }

    _stripeSuccess =  (item,token) =>{
      this.setState({loading_status:true})

      var formData = new FormData();
    
     formData.append('booking_id', item.id);
     formData.append('booking_code', item.booking_code);
     formData.append('transaction_number', token);
     formData.append('currency', item.currency);
     formData.append('total_amount', parseInt(item.basic_price) + parseInt(item.convenience_fees) + parseInt(item.discount) + parseInt(item.publish_price));
     formData.append('payment_date', this.convertDate(new Date()));


      let url = urls.base_url +'booking/stripe_success'
                    fetch(url, {
                    method: 'POST',
                    body:formData,
                    headers: {
                       'token': this.props.user.token,
                    }
                    }).then((response) => response.json())
                        .then((responseJson) => {
                          console.log("*********",responseJson)
                          this.setState({loading_status:false})

                          if(responseJson.status){
                              this.props.navigation.pop()
                              showMessage("Payment Done successfully !")
                          }
                          else{
                            
                          }
                          
                         
                      }
                        ).catch((error) => {
                          this.setState({loading_status:false})

                         
                        });
    
    
    
    }

    _stripeFail =  (item) =>{
      this.setState({loading_status:true})

      var formData = new FormData();
    
     formData.append('booking_id', item.id);
     formData.append('booking_code', item.booking_code);
     formData.append('payment_date', this.convertDate(new Date()));


      let url = urls.base_url +'booking/stripe_fail'
                    fetch(url, {
                    method: 'POST',
                    body:formData,
                    headers: {
                       'token': this.props.user.token,
                    }
                    }).then((response) => response.json())
                        .then((responseJson) => {
                          console.log("*********",responseJson)
                          this.setState({loading_status:false})

                          if(responseJson.status){
                              this.props.navigation.pop()
                          }
                          else{
                            
                          }
                          
                         
                      }
                        ).catch((error) => {
                          this.setState({loading_status:false})

                         
                        });
    
    
    
    }
    _stripe = async (item)=>{
      console.log('Paying for---',item)
      const options = {
        requiredBillingAddressFields: 'full',
        prefilledInformation: {
          email:this.props.user.email ,
          billingAddress: {
            name: this.props.user.name,
            line1:'',
            line2: '',
            city: '',
            state:'',
            country: '',
            postalCode: '',
          },
        },
      }
    
      const token = await stripe.paymentRequestWithCardForm(options)
      var cardId = token.card.cardId
      var tokenId = token.tokenId
      if (tokenId) this._stripeSuccess(item,tokenId)
      else this._stripeFail(item)
  }

    _payment =(item)=>{
        var type = item.payment_gateway
        var approveDate = item.supplier_approve_time
        
        if(type == 1) this._razorPay(item)
        else if(type == 2) this._stripe(item)
    }

    _renderRequest = ({item, index}) =>{
        //admin_status  0 - pending from admin side 1 admin approved the booking
        //status 0 -pending 1 Approved 2 Paid 3 Completed
        //payment_status - pending or paid
        //booking_type_name - this will be the type of booking like package, hotel, transport, native, tour to transfer
        return (
           <RequestComponent
            object = {item} 
            onPay={this._payment}
            index ={index}/>
        )
      }


    render() {

        return (
          <SafeAreaView style={{flex:1,backgroundColor:colors.LIGHT_BLACK}}>
         
          {/** header */}
          <View style={styles.header}>
          <View style={{flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
          {/* <TouchableOpacity
          onPress={()=> {this.props.navigation.goBack()} }>

          <FastImage 
          source={require('../assets/round_back_black.png')}
          style={{height:40,width:40}} 
          resizeMode={FastImage.resizeMode.contain}/>
           </TouchableOpacity> */}
           <View style={{margin:10,flexDirection:'row',alignItems:'center',marginTop:10}}>
                <FastImage 
                source={require('../assets/chat_yellow.png')}
                style={{height:30,width:30}} 
                resizeMode={FastImage.resizeMode.contain}/>
                <Text style={{color:colors.COLOR_PRIMARY,fontSize:18,margin:10}}>Requests</Text>

         </View>


            <TouchableOpacity onPress={() => this._initialize()}>
            <FastImage 
                source={require('../assets/plus-empty-yellow.png')}
                style={{height:25,width:25,margin:10}} 
                resizeMode={FastImage.resizeMode.cover}/>
            </TouchableOpacity>

           </View>

          <View style={{margin:12,marginTop:25}}>
            <Text style={{fontWeight:'100',fontSize:14,marginVertical:5,color:'grey',width:'80%'}}>The details of your request and the complete costs associated with it are listed below.Please take a look an confirm your request below.</Text> 
               
          </View>
          </View>

            {/** header end */}

            <View style={{height:10}}/>



               {/** hotels */}
             
              
                <FlatList
                    style={{marginVertical:0}}
                    horizontal={false}
                    numColumns={1}
                    data={this.state.requests}
                    showsHorizontalScrollIndicator={false}
                    renderItem={(item,index) => this._renderRequest(item,index)}
                    keyExtractor={item => item.id}
                />

               

              

              {/** hotels end */}

            

         </SafeAreaView>




        )
    }
}

const mapStateToProps = state => {
	return {

		user: state.user,
	
	};
};



export default connect(mapStateToProps, null)(Requests);

let styles = StyleSheet.create({
  header:{
    
       backgroundColor:colors.BLACK,
       borderBottomRightRadius:20,
       height:dimensions.SCREEN_HEIGHT * 0.25
  },
 
  bottomContainer:{
    position:'absolute',
    bottom:0,left:0,right:0,
    height:45,
     backgroundColor:'rgba(30,30,30,0.7)',
   // backgroundColor:'rgba(0,0,0,0.91)',
   borderTopColor:'black',
   borderTopWidth:0.7

  
  },
  blackContainer:{
    width:'95%',
    paddingHorizontal:5,
    paddingVertical:10,
    backgroundColor:colors.BLACK,
    borderRadius:20,
    marginVertical:5,
    alignSelf:'center',
  
   
  },
  yellowText:{
    color:colors.COLOR_PRIMARY
  },
  whiteText:{
    color:colors.WHITE
  },
  rowContainer:{
    flexDirection:'row',
   // justifyContent:'space-between',
    alignItems:'center',
   
},
buttonContainer:{
  borderRadius:20,
  backgroundColor:colors.BLACK,
  padding:10,
  justifyContent:'center',
  alignItems: 'center',
  borderWidth:0.5,borderColor:colors.COLOR_PRIMARY,
  marginBottom:10,
  width:150,alignSelf:'center'
}

}
)
