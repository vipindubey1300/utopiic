import React, {Component} from 'react';
import {Text, View, Image,Dimensions,
  Alert,ToastAndroid,StatusBar,BackHandler,
  ScrollView,StyleSheet,Platform,
  ImageBackground,TouchableOpacity,
  ActivityIndicator,TextInput,FlatList,SafeAreaView} from 'react-native';
import { colors } from '../app_constants';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'

import AsyncStorage from '@react-native-community/async-storage';
import { withNavigationFocus } from 'react-navigation';
// { Indicator, Pages } from 'react-native-pages';

import SwiperFlatList from 'react-native-swiper-flatlist';



import PreferencesComponent from '../components/PreferencesComponent';
import BottomBarSupplier from '../components/BottomBarSupplier';
import HomeDataSupplier from './HomeDataSupplier';
import Suppliers from './Suppliers';
import {showMessage,showToastMessage} from '../utils/showMessage';
import { connect } from 'react-redux';
import ProgressBar from '../components/ProgressBar';
import Swiper from '@starodubenko/react-native-deck-swiper'

import {  urls } from '../app_constants';

 const d = Dimensions.get("window")



class HomeSupplier extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          loading_status:false,
          selectedTab:'home',
       

      };

    }


    componentDidMount(){
        const selectedTab = this.bar.getSelectedTab()
         this.setState({selectedTab})
     }

     _onTabSelect = (selectedTab) =>{
        // console.log('selectedTab------',selectedTab)//home//information
         this.setState({selectedTab})
     }

     _getView =()=>{
         if(this.state.selectedTab == 'home'){
             return(
                <HomeDataSupplier  {...this.props}/>
             )
         }
         else if(this.state.selectedTab == 'suppliers'){
            return(
                <Suppliers  {...this.props}/>
            )
        }
        else if(this.state.selectedTab == 'request'){
            return(
                <Text style={{color:'white',margin:50}}>Chat Feature Coming Soon</Text>
            )
        }
        else{
            return(
                <HomeData/>
            )
        }
     }


    render() {

        return (
         <SafeAreaView style={{backgroundColor:'black',flex:1}}>
          <StatusBar
              backgroundColor="black"
              barStyle="light-content"
           />

             <View style={{flex:1}}>
             {this._getView()}
             
             </View>
        
             <BottomBarSupplier   handler={this._onTabSelect}
             ref={ref => this.bar = ref} {...this.props}/>
             
          { this.state.loading_status && <ProgressBar/> }

            </SafeAreaView>




        )
    }
}


const mapStateToProps = state => {
	return {
	
		user: state.user,
	
	};
};

const mapDispatchToProps = dispatch => {
  return {
   
    remove : (user_info) => dispatch(removeUser(user_info)),
   
    
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(HomeSupplier);

let styles = StyleSheet.create({
  container:{
        flex: 1,


  },
  imageBackground:{

    position: 'absolute',
    flex: 1,
    backgroundColor:'rgba(0,0,0,0.45)',
    width: d.width,

    zIndex:-2,
    height: d.height * 0.97,

  },
  heading:{
    fontSize:18,
    color:'white',
    fontWeight:'bold',
      textAlign:'center',
  },
  title:{
    fontSize:12,
    color:'white',
    textAlign:'center',
    marginBottom:20
  },
    input: {
    width: d.width * 0.64,
		marginHorizontal: 40,
		paddingTop: 0,
		marginTop: 0,
  },
  buttonContainer:{
    marginTop:10,
    padding:10,
    width:null,
    height:null,
    alignSelf:'center',
    flexDirection:'row',
    backgroundColor:'#BE863A',
    alignItems:'center'

}

}
)
