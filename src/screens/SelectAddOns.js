import React, {Component} from 'react';
import {Text, View, Image,Dimensions,
  Alert,ToastAndroid,StatusBar,BackHandler,
  ScrollView,StyleSheet,Platform,
  ImageBackground,TouchableOpacity,ActivityIndicator,TextInput,FlatList,SafeAreaView} from 'react-native';
import { colors ,urls,dimensions} from '../app_constants';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import ButtonComponent from '../components/ButtonComponent';

import AsyncStorage from '@react-native-community/async-storage';
import { withNavigationFocus } from 'react-navigation';
// { Indicator, Pages } from 'react-native-pages';
import FastImage from 'react-native-fast-image'
import DateTimePicker from 'react-native-modal-datetime-picker';

import SwiperFlatList from 'react-native-swiper-flatlist';


import { connect } from 'react-redux';

import BannerComponent from '../components/BannerComponent';
import AddOnsComponent from '../components/AddOnsComponent';

 const d = Dimensions.get("window")



 const guest_count=[
   {id:1,value:'1'},
   {id:2,value:'2'},
   {id:3 ,value:'3'}
 ]
 class SelectAddOns extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
         addons:[],
          hotel_id:0,
          from_date:'',
          to_date:'',
          selectedAddOns:[],
          selectedGuest:null

      };

    }

   
    _fetchAddOns  =  () =>{
     
      this.setState({loading_status:true})
    
               let url = urls.base_url +'hotels/list_products'
                    fetch(url, {
                    method: 'POST',
                    headers: {
                       'token': this.props.user.token,
                    }
    
                    }).then((response) => response.json())
                        .then((responseJson) => {
                          console.log("*********",responseJson)
                         
                          this.setState({loading_status:false})

                          if(responseJson.status){
                            var list = responseJson.data.list
                            console.log('rooms,',list)
                           
                           
                            this.setState({addons:list})
                          }
                          else{
                            
                          }
                          
                         
                      }
                        ).catch((error) => {
                          this.setState({loading_status:false})

                         
                        });
    
    
    
    }




  componentWillMount(){
   // console.log(JSON.stringify(this.props.user))
   var result = this.props.navigation.getParam('result')
   var hotel = result['hotel']
   var from_date = result['from_date']
   var to_date = result['to_date']
   var room = result['room']
   console.log('hotel_id-----',result['hotel'])
   console.log('from_date-----',result['from_date'])
   console.log('to_date-----',result['to_date'])
   console.log('room_id-----',result['room'])


   this.setState({
    hotel,from_date,to_date
   })
    
 
   this._fetchAddOns(hotel.id)
   
  }


   
  onHotelClick = (item) =>{
    console.log('onHotelClick--------',JSON.stringify(item))
      var id = item.id
      let obj ={'hotel_id':id}
      this.props.navigation.navigate("HotelDetails",{result:obj})
  
  }



        onPressAction = (item) => {
            this.setState({selectedAddOns:item})
     }


  _renderAddOn = ({item, index}) =>{
    const { selectedAddOns } = this.state;
    let isSelected =
      selectedAddOns.filter(o => {
        return o.id === item.id;
      }).length > 0
        ? true
        : false;
    return (
      <AddOnsComponent
        object = {item} 
        index ={index}
        onSelectAddOns={this._didSelectAddOns}
        selected={isSelected} />
   )
    }

    _didSelectAddOns = selectedItem => {
        console.log("SElected item----->",selectedItem)
        console.log("SElected item List----->", this.state.selectedAddOns)

        let selectedAddOns = this.state.selectedAddOns;
        let isItemSelected =
          selectedAddOns.filter(item => {
            return item.id.includes(selectedItem.id);
          }).length > 0
            ? true
            : false;
    
        if (isItemSelected) {
          const index = selectedAddOns.findIndex(
            obj => obj.id === selectedItem.id
          );
          selectedAddOns.splice(index, 1);
        } else {
          selectedAddOns.push(selectedItem);
        }
    
        this.setState({ selectedAddOns });
      };



      _onDone =()=>{
        var result =   this.props.navigation.getParam('result')

        let obj ={
          'hotel':result.hotel,
          'from_date':result.from_date,
          'to_date':result.to_date,
          'room':result.room,
          'add_on':this.state.selectedAddOns
        }
        this.props.navigation.navigate("Review",{result:obj})
      }
      _onSkip =()=>{
        var result =   this.props.navigation.getParam('result')

        let obj ={
          'hotel':result.hotel,
          'from_date':result.from_date,
          'to_date':result.to_date,
          'room':result.room,
          'add_on':[]
        }
        this.props.navigation.navigate("Review",{result:obj})
      }
   
    render() {

        return (
          <SafeAreaView style={{flex:1,backgroundColor:'black'}}>
          <ScrollView style={{backgroundColor:colors.LIGHT_BLACK}}>

    


          {/** header */}
          <View style={styles.header}>
          <View style={{flexDirection:'row',alignItems:'center'}}>
          <TouchableOpacity
          onPress={()=> {this.props.navigation.goBack()} }>
          <FastImage 
          source={require('../assets/round_back_black.png')}
          style={{height:40,width:40}} 
          resizeMode={FastImage.resizeMode.contain}/>
           </TouchableOpacity>
           <Text style={{color:colors.COLOR_PRIMARY,fontSize:18,marginHorizontal:10}}>Select Add Ons </Text> 

           </View>

          <View style={{margin:12,marginTop:40}}>
            <Text style={{fontWeight:'100',fontSize:14,marginVertical:5,color:'white',width:'80%'}}>Take a look at some of the add on packages we offer.If you wish to skip selecting any additional packages,you can simply press the skip button.</Text> 
            <ButtonComponent 
            style={{marginTop:20,width:dimensions.SCREEN_WIDTH * 0.34,alignSelf:'flex-end'}}
            handler={this._onSkip}
            label ={'SKIP >>>'}/>            
          </View>
          </View>

            {/** header end */}


                
           

               {/** rooms */}
               <View>
              
                <FlatList
                    style={{marginVertical:10,backgroundColor:colors.BLACK}}
                    horizontal={false}
                    data={this.state.addons}
                    showsHorizontalScrollIndicator={false}
                    renderItem={(item,index) => this._renderAddOn(item,index)}
                    keyExtractor={item => item.id}
                    //ListEmptyComponent={this._renderEmptyComponent}
                    extraData={this.props}

                />

               </View>

              {/** hotels end */}




    

             




            
              
              </ScrollView>

              <View style={styles.bottomContainer}>
              <ButtonComponent 
              style={{marginTop:-15,width:dimensions.SCREEN_WIDTH * 0.4,alignSelf:'center'}}
              handler={this._onDone}
              label ={'SELECT'}/>
          </View>


         </SafeAreaView>




        )
    }
}

const mapStateToProps = state => {
	return {

		user: state.user,
	
	};
};



export default connect(mapStateToProps, null)(SelectAddOns);

let styles = StyleSheet.create({
  header:{
    
       backgroundColor:colors.BLACK,
       borderBottomRightRadius:20,
       height:dimensions.SCREEN_HEIGHT * 0.3
  },
 
  
  rowContainer:{
      flexDirection:'row',
     // justifyContent:'space-between',
      alignItems:'center',
     
  },

  dateHeading:{
      color:colors.COLOR_PRIMARY,
      marginVertical:7
  },
  bottomContainer:{
    position:'absolute',
    bottom:0,left:0,right:0,
    height:45,
     backgroundColor:'rgba(30,30,30,0.7)',
   // backgroundColor:'rgba(0,0,0,0.91)',
   borderTopColor:'black',
   borderTopWidth:0.7

  
  },
  roomContainer:{
    width:'95%',
    paddingHorizontal:5,
    paddingVertical:10,
    backgroundColor:colors.BLACK,
    borderRadius:20,
    marginVertical:5,
    alignSelf:'center',
    borderWidth:1,
    borderColor:'black'
   
  },
  yellowText:{
    color:colors.COLOR_PRIMARY
  },
  whiteText:{
    color:colors.WHITE
  },
  rowContainer:{
    flexDirection:'row',
   // justifyContent:'space-between',
    alignItems:'center',
   
},

}
)
