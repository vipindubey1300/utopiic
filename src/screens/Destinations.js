import React, {Component} from 'react';
import {Text, View, Image,Dimensions,
  Alert,ToastAndroid,StatusBar,BackHandler,
  ScrollView,StyleSheet,Platform,
  ImageBackground,TouchableOpacity,ActivityIndicator,TextInput,FlatList,SafeAreaView} from 'react-native';
import { colors ,urls,dimensions} from '../app_constants';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'

import AsyncStorage from '@react-native-community/async-storage';
import { withNavigationFocus } from 'react-navigation';
// { Indicator, Pages } from 'react-native-pages';
import FastImage from 'react-native-fast-image'

import SwiperFlatList from 'react-native-swiper-flatlist';


import { connect } from 'react-redux';

import BannerComponent from '../components/BannerComponent';
import SearchInput from '../components/SearchInput';
import { showMessage } from '../utils/showMessage';

 const d = Dimensions.get("window")



 class Destinations extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          countries:[],
          loading_status:false,
      
         

      };

    }




    _fetchCountries = async () =>{
        console.log("*********",this.props.user.token)
      this.setState({loading_status:true})
               let url = urls.base_url +'general/counties'
                    fetch(url, {
                    method: 'POST',
                    // headers: {
                    //    'token': this.props.user.token,
                    // }
    
                    }).then((response) => response.json())
                        .then((responseJson) => {

                          console.log('countries,',responseJson)
                          this.setState({loading_status:false})

                          if(responseJson.status){
                            var list = responseJson.data.list
                            console.log('countries,',list)

                           this.setState({countries:list})
                          }
                          else{
                            
                          }
                          
                         
                      }
                        ).catch((error) => {
                          this.setState({loading_status:false})

                         
                        });
    
    
    
    }



    _searchCountries = async (text) =>{
      console.log("*********",this.props.user.token)
    this.setState({loading_status:true})
             let url = urls.base_url +'general/destination_search/'+text
                  fetch(url, {
                  method: 'POST',
                 
                  }).then((response) => response.json())
                      .then((responseJson) => {

                       
                        this.setState({loading_status:false})

                        if(responseJson.status){
                          var list = responseJson.data.list
                          console.log('countries,',list)
                          if(list.length == 0) showMessage('No countries Found .')
                          if(list.length != 0) this.setState({countries:list})
                        }
                        else{
                          
                        }
                        
                       
                    }
                      ).catch((error) => {
                        this.setState({loading_status:false})

                       
                      });
  
  
  
  }









    componentWillMount(){
   
     this._fetchCountries()
   
    }


    onCountryClick = (item) =>{
      console.log('onCountryClick--------',JSON.stringify(item))
      var id = item.id
      let obj ={'country_id':id}
      this.props.navigation.navigate("CountryDetails",{result:obj})
    
    }



     _renderCountries = ({item, index}) =>{
        return (
           <TouchableOpacity onPress={()=> this.onCountryClick(item)}
           style={styles.listContainer}>
           <FastImage 
           source={{uri:item.image}}
           style={styles.countryImage} 
           resizeMode={FastImage.resizeMode.cover}/>

           <View style={{alignItems:'center'}}>
           <Text style={styles.countryText}>{item.country.toUpperCase()}</Text>
           <Text style={{color:'white'}}>This is dummy short info </Text>
           </View>
           </TouchableOpacity>
        )
      }

      _onSearch =()=>{
        var searchText = this.searchInput.getInputValue()
        console.log(searchText)
        this._searchCountries(searchText)

      }

         
    onApplyFilter = data => {
      console.log("DATA---",JSON.stringify(data))
      //this.setState({filters_data:data})
     // this._applyFilterExperiences(data)

    };


      _onFilter =()=>{
        this.props.navigation.navigate("Filter" ,{ onApply: this.onApplyFilter })
      }
  

    render() {

        return (
          <ScrollView bounces={false}
          style={{backgroundColor:'black',flex:1}}>
          {/** header */}
          <View style={styles.header}>
          <View/>
          </View>

            {/** header end */}


            {/** heading x*/}
                <View style={{margin:10,flexDirection:'row',alignItems:'center',marginTop:-10}}>
                <FastImage 
                source={require('../assets/destination_yellow.png')}
                style={{height:30,width:30}} 
                resizeMode={FastImage.resizeMode.cover}/>
                <Text style={{color:colors.COLOR_PRIMARY,fontSize:18,margin:10}}>The World</Text>

                </View>
                <Text style={{color:'white',fontSize:12,margin:10,width:'60%'}}>The world at your fingertips.Explore countries and cities.</Text>

                <SearchInput
                placeholder={'Search Destinations..'}
                 onSubmitEditing={()=> this._onSearch()}
                onFinish={this._verifyOtp}
                inputRef={ref => this.search = ref}
                ref={ref => this.searchInput = ref}
                style={{width:'90%',alignSelf:'center',marginVertical:10}}
                returnKeyType="go"
                onSearch={this._onSearch}
                onFilter={this._onFilter}
               />
             {/** heading end */}



               {/** exclusive */}
               <View style={[styles.countriesContainer,{
                 height:this.state.countries.length > 0 ? 'auto' : dimensions.SCREEN_HEIGHT * 0.8
               }]}>
              


                <FlatList
                    style={{marginVertical:10}}
                    horizontal={false}
                    data={this.state.countries}
                    showsHorizontalScrollIndicator={false}
                    renderItem={(item,index) => this._renderCountries(item,index)}
                    keyExtractor={item => item.id}
                />

               </View>

              {/** exclusive end */}




              
              
         
         </ScrollView>




        )
    }
}

const mapStateToProps = state => {
	return {

		user: state.user,
	
	};
};



export default connect(mapStateToProps, null)(Destinations);

let styles = StyleSheet.create({
  header:{
        flexDirection: 'row',
        justifyContent:'space-between',
        alignItems:'center',
        height:30
  },
  countriesContainer:{
      paddingVertical:10,
      paddingHorizontal:2,
      width:dimensions.SCREEN_WIDTH,
      backgroundColor:colors.LIGHT_BLACK,
      marginTop:10
  },
  listContainer:{
  width:'95%',
  borderColor:colors.COLOR_PRIMARY,
  height:160,
  marginVertical:5,
  alignSelf:'center',
  overflow:'hidden',justifyContent:'center',alignItems:'center',
},
countryImage:{
    position:'absolute',
    top:0,
    left:0,
    right:0,
    bottom:0,
},
countryText:{
    color:'white',
    fontWeight:'bold',
    fontSize:19,
}


}
)
