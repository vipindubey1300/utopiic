import React, {Component} from 'react';
import {Text, View, Image,Dimensions,
  Alert,ToastAndroid,StatusBar,BackHandler,
  ScrollView,StyleSheet,Platform,
  ImageBackground,TouchableOpacity,ActivityIndicator,TextInput,FlatList,SafeAreaView} from 'react-native';
import { colors ,urls,dimensions} from '../app_constants';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import ButtonComponent from '../components/ButtonComponent';
import {showMessage,showToastMessage} from '../utils/showMessage';

import AsyncStorage from '@react-native-community/async-storage';
import { withNavigationFocus } from 'react-navigation';
// { Indicator, Pages } from 'react-native-pages';
import FastImage from 'react-native-fast-image'
import DateTimePicker from 'react-native-modal-datetime-picker';

import SwiperFlatList from 'react-native-swiper-flatlist';
import { Rating, AirbnbRating } from 'react-native-elements';
import { Checkbox } from 'react-native-paper';


import { connect } from 'react-redux';

import BannerComponent from '../components/BannerComponent';
import AddOnsComponent from '../components/AddOnsComponent';
import Message from '../components/Message';

 const d = Dimensions.get("window")



 class Review extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          add_on:[],
          hotel:'',
          from_date:'',
          to_date:'',
          room:'',
          days:0,
          addOnPrice:0,
          isBooked:false,
          checked:false

      };

    }

  
   days_between(date1, date2) {

      // The number of milliseconds in one day
      const ONE_DAY = 1000 * 60 * 60 * 24;
  
      // Calculate the difference in milliseconds
      const differenceMs = Math.abs( new Date(date1) -  new Date(date2));
  
      // Convert back to days and return
      return Math.round(differenceMs / ONE_DAY);
  
  }

  componentWillMount(){
   // console.log(JSON.stringify(this.props.user))
   var result = this.props.navigation.getParam('result')
   var hotel = result['hotel']
   var from_date = result['from_date']
   var to_date = result['to_date']
   var room = result['room']
   var add_on = result['add_on']
   const days = this.days_between(from_date,to_date)



   console.log('hotel_id-----',result['hotel'])
   console.log('from_date-----',result['from_date'])
   console.log('to_date-----',new Date(result['to_date']))
   console.log('room_id-----',result['room'])
   console.log('add_on-----',result['add_on'])
   console.log('days-----',days)

   var addOnPrice = 0 
   add_on.map((object) => {
    addOnPrice = addOnPrice + parseInt(object.price)
});


   this.setState({
    hotel,from_date,to_date,room,add_on,days,addOnPrice
   })
  }





   

  convertDate(date) {
    var yyyy = date.getFullYear().toString();
    var mm = (date.getMonth()+1).toString();
    var dd  = date.getDate().toString();

    var mmChars = mm.split('');
    var ddChars = dd.split('');

    return yyyy + '-' + (mmChars[1]?mm:"0"+mmChars[0]) + '-' + (ddChars[1]?dd:"0"+ddChars[0]);
  }


      _onDone =()=>{

        var result = this.props.navigation.getParam('result')
       var hotel = result['hotel']
        var from_date = result['from_date']
        var to_date = result['to_date']
        var room = result['room']
        var add_on = result['add_on']
        const days = this.days_between(from_date,to_date)

        var tempString = ''
        for(let i = 0; i < add_on.length; i++){
            tempString = tempString + add_on[i].id + ','
        }


      var formData = new FormData();
      formData.append('hotel_id',hotel.id);
      formData.append('hotel_room_id',room.id);
      formData.append('currency',hotel.currency);
      formData.append('basic_price',hotel.price);
      formData.append('convenience_fees',hotel.price);
      formData.append('discount',10);
      formData.append('tax',10);
      formData.append('days',days);
      formData.append('date',this.convertDate(new Date(from_date)));
      formData.append('addon',tempString);

      console.log('formData----',formData)
      this.setState({loading_status:true})
    
               let url = urls.base_url +'hotels/booking'
          
                    fetch(url, {
                    method: 'POST',
                    body:formData,
                    headers: {
                      
                      //'Content-Type': 'application/x-www-form-urlencoded',
                      'token': this.props.user.token,
                    }
    
                    }).then((response) => response.json())
                        .then((responseJson) => {

                          console.log("))))))))))",responseJson)
                          
                         // this.setState({isBooked: true})
                          if(responseJson.status){
                            // setTimeout(() => {
                            //   this.setState({isBooked: false})
                            //   this.props.navigation.navigate("Home")
                            // }, 4000)

                          
                            showMessage("Booked Successfully !",false)
                            this.props.navigation.navigate("Home")
                          }
                          else{
                            ToastAndroid.show(responseJson.message,ToastAndroid.SHORT)
                          }
                          
                         
                      }
                        ).catch((error) => {
                          this.setState({loading_status:false})
                          ToastAndroid.show(error.message,ToastAndroid.SHORT)
                         //console.log(error)
                        });
    
    
    
      }

      _onFinal =()=>{
        if(this.state.checked) this._onDone()
        else showMessage('Accept Terms and Condition to continue')
      }


      _onSkip =()=>{
        var result =   this.props.navigation.getParam('result')

        let obj ={
          'hotel':result.hotel,
          'from_date':result.from_date,
          'to_date':result.to_date,
          'room':result.room,
          'add_on':[]
        }
        this.props.navigation.navigate("Review",{result:obj})
      }


      _addOns = (add_on) =>{
           
        return add_on.map((object) => {
          return (
           <View style={[styles.rowContainer,{marginHorizontal:3,justifyContent:'space-between'}]}>
           <Text style={[styles.yellowText ,{fontSize:12}]}>{object.name}</Text>

             <FastImage 
             source={{uri:object.image}}
             style={{height:55,width:55,margin:3,borderRadius:12,overflow:'hidden'}} 
             resizeMode={FastImage.resizeMode.contain}/>
           </View>
            
          );
      });
 
     
  }

  _terms =()=>{
    this.props.navigation.navigate("Terms")
  }
   
    render() {
      const {hotel,room,add_on,from_date,to_date} = this.state

        return (
          <SafeAreaView style={{flex:1,backgroundColor:'black'}}>
          <ScrollView style={{backgroundColor:colors.LIGHT_BLACK}}>

    


          {/** header */}
          <View style={styles.header}>
          <View style={{flexDirection:'row',alignItems:'center'}}>
          <TouchableOpacity
          onPress={()=> {this.props.navigation.goBack()} }>
          <FastImage 
          source={require('../assets/round_back_black.png')}
          style={{height:40,width:40}} 
          resizeMode={FastImage.resizeMode.contain}/>
           </TouchableOpacity>
           <Text style={{color:colors.COLOR_PRIMARY,fontSize:18,marginHorizontal:10}}>Review</Text> 

           </View>

          <View style={{margin:12,marginTop:40}}>
            <Text style={{fontWeight:'100',fontSize:14,marginVertical:5,color:'white',width:'80%'}}>The details of your request and the complete costs associated with it are listed below.Please take a look an confirm your request below.</Text> 
               
          </View>
          </View>

            {/** header end */}

            <View style={{height:40}}/>



              {/** hotel */}
            <View style={styles.blackContainer}>
               <View style={[styles.rowContainer,{justifyContent:'space-between',alignItems:'flex-start'}]}>
               <View style={{width:'60%'}}>
               <Text style={[styles.whiteText,{marginVertical:3}]}>Hotel</Text>
               <Text style={[styles.yellowText,{marginVertical:3}]}>{hotel.name}</Text>
               <Text style={[styles.yellowText,{fontSize:10,marginVertical:3}]}>{hotel.address}</Text>
               <View style={{flexDirection:'row',alignItems:'center'}}>
                <Rating
                type='custom'
                readonly={true}
                ratingColor={colors.COLOR_PRIMARY}
                ratingBackgroundColor='rgba(0,0,0,0.00000000001)'
                ratingCount={hotel.star_rating}
                imageSize={14}
                startingValue={4}
                tintColor= 'rgba(0,0,0,0.9)'
                style={{ paddingVertical: 0 ,backgroundColor:'rgba(0,0,0,0.0000001)'}}
              />
              <Text style={{color:'grey'}}>({hotel.star_rating_count})</Text>
               </View>
                    
               </View>

               <FastImage 
               source={{uri:hotel.cover}}
               style={{height:90,width:90,margin:3,borderRadius:15}} 
               resizeMode={FastImage.resizeMode.cover}/>
               </View>



              <View style={{marginVertical:13}}>
                <Text style={[styles.whiteText,{marginVertical:3}]}>Rooms</Text>

                <Text style={[styles.yellowText,{marginVertical:3}]}>{room.name}</Text>


                <Text style={[{marginVertical:3,color:'grey'}]}>From :  
                <Text style={[styles.whiteText,{marginVertical:3}]}> {from_date}</Text>
                </Text>

                <Text style={[{marginVertical:3,color:'grey'}]}>Until :  
                <Text style={[styles.whiteText,{marginVertical:3}]}> {to_date}</Text>
                </Text>
              </View>



              <View style={{marginVertical:13}}>
              <Text style={[styles.whiteText,{marginVertical:3}]}>Price</Text>

              <View style={[styles.rowContainer,{marginVertical:3,justifyContent:'space-between'}]}>
              <Text style={[styles.yellowText,{marginVertical:3,fontSize:15}]}>Base Price </Text>
              <Text style={[styles.yellowText,{marginVertical:3,fontSize:15}]}>{hotel.currency} {hotel.price} </Text>
              </View>

               </View>

               <View style={{
                 width:'100%',
                 borderRadius:20,
                 borderWidth:0.5,
                 borderColor:colors.COLOR_PRIMARY,
                 justifyContent:'center',
                 alignItems:'center',
                 padding:15
               }}>
               <Text style={[styles.yellowText,{fontSize:15}]}>{hotel.currency} {hotel.price} </Text>

               </View>

            </View>
             {/** hotel end */}



            


              {/** add on */}

              {
                this.props.navigation.getParam('result').add_on.length > 0
                ?
                <View style={styles.blackContainer}>
            <Text style={[styles.whiteText,{marginVertical:3}]}>Selected Add On</Text>

            <View style={{marginVertical:10}}>
            {this._addOns(add_on)}
             </View>

             <View style={[styles.rowContainer,{marginVertical:3,justifyContent:'space-between'}]}>
              <Text style={[styles.whiteText,{marginVertical:3,fontSize:15}]}> Total Price </Text>
              <Text style={[styles.yellowText,{marginVertical:3,fontSize:15}]}> {add_on.length > 0 ? add_on[0].currency : ''}  {this.state.addOnPrice} </Text>
              </View>




           </View>
           :null

              }
            
            {/** add on end */}


            <Text style={[styles.yellowText,{marginVertical:3,fontSize:17,alignSelf:'center'}]}>TOTAL</Text>

            <View style={{
              width:'95%',
              borderRadius:20,
              borderWidth:0.5,
              borderColor:colors.COLOR_PRIMARY,
              justifyContent:'center',
              alignItems:'center',
              padding:15,
              backgroundColor:'black',alignSelf:'center'
            }}>
            <Text style={[styles.yellowText,{fontSize:15}]}> {hotel.currency} {parseFloat(parseInt(hotel.price) + parseInt(this.state.addOnPrice)).toFixed(2)}</Text>

            </View>



            <View style={[styles.rowContainer,{marginTop:27,alignSelf:'center'}]}>
               

               <TouchableOpacity onPress={()=> this._terms()}
                 style={[styles.buttonContainer,styles.rowContainer]}>
                    <Checkbox.Android
                     color={colors.COLOR_PRIMARY}
                     uncheckedColor={'white'}
                     onPress={() => this.setState({ checked: !this.state.checked })}
                     status={this.state.checked ? 'checked' : 'unchecked'}
                   />
 
                 <Text style={{color:colors.COLOR_PRIMARY,textAlign:'center'}}>   Terms and Conditions</Text>
                 </TouchableOpacity>
            
             </View>
           




                  <View style={{height:60}}/>
           

              </ScrollView>

              <View style={styles.bottomContainer}>
              <ButtonComponent 
              style={{marginTop:-15,width:dimensions.SCREEN_WIDTH * 0.45,alignSelf:'center'}}
              handler={this._onFinal}
              label ={'Confirm Request'}/>
          </View>


          {this.state.isBooked && <Message 
          success={true} message={'Requested Successfully'}/>}


         </SafeAreaView>




        )
    }
}

const mapStateToProps = state => {
	return {

		user: state.user,
	
	};
};



export default connect(mapStateToProps, null)(Review);

let styles = StyleSheet.create({
  header:{
    
       backgroundColor:colors.BLACK,
       borderBottomRightRadius:20,
       height:dimensions.SCREEN_HEIGHT * 0.3
  },
 
  bottomContainer:{
    position:'absolute',
    bottom:0,left:0,right:0,
    height:45,
     backgroundColor:'rgba(30,30,30,0.7)',
   // backgroundColor:'rgba(0,0,0,0.91)',
   borderTopColor:'black',
   borderTopWidth:0.7

  
  },
  blackContainer:{
    width:'95%',
    paddingHorizontal:5,
    paddingVertical:10,
    backgroundColor:colors.BLACK,
    borderRadius:20,
    marginVertical:5,
    alignSelf:'center',
  
   
  },
  yellowText:{
    color:colors.COLOR_PRIMARY
  },
  whiteText:{
    color:colors.WHITE
  },
  rowContainer:{
    flexDirection:'row',
   // justifyContent:'space-between',
    alignItems:'center',
   
},
buttonContainer:{
  borderRadius:20,
  backgroundColor:colors.BLACK,
  padding:10,
  justifyContent:'center',
  alignItems: 'center',
  borderWidth:0.5,borderColor:colors.COLOR_PRIMARY,
  marginBottom:10,
  //width:200,
  alignSelf:'center'
}

}
)
