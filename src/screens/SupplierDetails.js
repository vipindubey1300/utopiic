import React, {Component} from 'react';
import {Text, View, Image,Dimensions,
  Alert,ToastAndroid,StatusBar,BackHandler,
  ScrollView,StyleSheet,Platform,
  ImageBackground,TouchableOpacity,ActivityIndicator,TextInput,FlatList,SafeAreaView,Animated} from 'react-native';
import { colors ,urls,dimensions} from '../app_constants';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'

import AsyncStorage from '@react-native-community/async-storage';
import { withNavigationFocus } from 'react-navigation';
// { Indicator, Pages } from 'react-native-pages';
import FastImage from 'react-native-fast-image'

import SwiperFlatList from 'react-native-swiper-flatlist';
//import { Rating, AirbnbRating } from 'react-native-elements';
import { Rating, AirbnbRating } from 'react-native-ratings';

import ButtonComponent from '../components/ButtonComponent';

import { connect } from 'react-redux';

import BannerComponent from '../components/BannerComponent';
import ExperienceComponent from '../components/ExperienceComponent';

 const d = Dimensions.get("window")



 class SupplierDetails extends React.Component {
    constructor(props) {
        super(props);
        
        this.state = {
        
          loading_status:false,
          profile:''
         
        }
    }

    _fetchProfile =  () =>{
        console.log("*********",this.props.user.token)
        this.setState({loading_status:true})
        var result  = this.props.navigation.getParam('result')
        var id = result.supplier_id
        var formData = new FormData();
        
        formData.append('supplier_id',id);
        formData.append('user_type', 6);

        console.log("*********",formData)
      
                 let url = urls.base_url +'suppliers/native/public_profile'
                      fetch(url, {
                      method: 'POST',
                      body: formData,
                      headers: {
                         'token': this.props.user.token,
                      }
      
                      }).then((response) => response.json())
                          .then((responseJson) => {
                            console.log("*********",responseJson)
                           
                            this.setState({loading_status:false})
  
                            if(responseJson.status){
                             
                             
                             this.setState({profile:responseJson.data})
                            }
                            else{
                              
                            }
                            
                           
                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})
  
                           
                          });
      
      
      
      }




    componentWillMount(){
 
        this._fetchProfile()
    }


     
   

    render() {
        const{profile} = this.state

        return (
     <SafeAreaView style={{flex:1,backgroundColor:'black'}}>
          <ScrollView style={{backgroundColor:'black'}}>
          

            <View style={{height: dimensions.SCREEN_HEIGHT * 0.33,width:'100%',backgroundColor:'grey'}}>
            
            <FastImage 
            source={{uri:profile.profile}}
            style={{height:'100%',width : '100%'}} 
            resizeMode={FastImage.resizeMode.cover}/>
            </View>

            <View style={styles.middleContainer}>
            <TouchableOpacity style={{flex:1,justifyContent:'center',
           borderBottomColor:colors.COLOR_PRIMARY,
            borderBottomWidth:1,backgroundColor:'rgba(0,0,0,0.5)',paddingLeft:20}}>
            <Text style={{color:colors.COLOR_PRIMARY}}>{profile.name}</Text>
            <View style={{flexDirection:'row',alignItems:'center'}}>
            <Rating
                    type='custom'
                     ratingColor={colors.COLOR_PRIMARY}
                    ratingBackgroundColor='rgba(0,0,0,0.00000000001)'
                    ratingCount={profile.rating}
                    imageSize={14}
                    startingValue={4}
                    tintColor= 'rgba(0,0,0,0.1)'
                    style={{ paddingVertical: 0 }}
          />
          <Text style={{color:'grey'}}>  ({profile.rating})</Text>
            </View>
            </TouchableOpacity>
            <TouchableOpacity style={{flex:1,justifyContent:'center',alignItems:'center',backgroundColor:colors.COLOR_PRIMARY}}>
                <Text>Contact</Text>
            </TouchableOpacity>
            </View>







               {/** profiles */}
               <View style={styles.profileContainer}>

               <View style={styles.rowContainer}>
                    <FastImage 
                    source={require('../assets/pin.png')}
                    style={styles.image} 
                    resizeMode={FastImage.resizeMode.contain}/>
                    <Text style={styles.labelText}>Lives in </Text>
                    <Text style={styles.valueText}>{profile.location} </Text>
               </View>


               <View style={styles.rowContainer}>
               <FastImage 
               source={require('../assets/speaks.png')}
               style={styles.image} 
               resizeMode={FastImage.resizeMode.contain}/>
               <Text style={styles.labelText}>Speaks </Text>
               <Text style={styles.valueText}>{profile.lenguage} </Text>
             </View>



             <View style={styles.rowContainer}>
             <FastImage 
             source={require('../assets/favorite.png')}
             style={styles.image} 
             resizeMode={FastImage.resizeMode.contain}/>
             <Text style={styles.labelText}>Loves </Text>
             <Text style={styles.valueText}>{profile.love} </Text>
           </View>



           <View style={styles.rowContainer}>
           <FastImage 
           source={require('../assets/verified_yellow.png')}
           style={styles.image} 
           resizeMode={FastImage.resizeMode.contain}/>
           <Text style={styles.labelText}>Verified </Text>
           <Text style={styles.valueText}>Native Hosts</Text>
         </View>


         <View style={styles.rowContainer}>
           <FastImage 
           source={require('../assets/update.png')}
           style={styles.image} 
           resizeMode={FastImage.resizeMode.contain}/>
           <Text style={styles.labelText}>Average response time is </Text>
           <Text style={styles.valueText}>less than 7 hours</Text>
         </View>

              
              
               </View>

              {/** profiles end */}

              
         
         </ScrollView>
         {/** header */}
         <TouchableOpacity style={styles.header}
         onPress={()=> {this.props.navigation.goBack()} }>
           <FastImage 
           source={require('../assets/back.png')}
           style={{height:30,width:30,marginLeft:4,marginRight:10}} 
           resizeMode={FastImage.resizeMode.contain}/>
          </TouchableOpacity>

  

           {/** header end */}


           <View style={styles.bottomContainer}>
           <ButtonComponent 
           style={{marginTop:-15,width:dimensions.SCREEN_WIDTH * 0.4,alignSelf:'center'}}
          // handler={this._onDone}
           label ={'Share'}/>
       </View>

         </SafeAreaView>


        )
    }
}

const mapStateToProps = state => {
	return {

		user: state.user,
	
	};
};



export default connect(mapStateToProps, null)(SupplierDetails);

let styles = StyleSheet.create({
  header:{
      position:'absolute',
      top:0,
      left:0
  },
 
  profileContainer:{
    paddingVertical:10,
    paddingHorizontal:2,
    width:dimensions.SCREEN_WIDTH,
    backgroundColor:colors.BLACK,
    marginTop:25,
   
},
  rowContainer:{
      flexDirection:'row',
     // justifyContent:'space-between',
      alignItems:'center',
      marginVertical:7
  },
  middleContainer:{
    height:90,
    width:dimensions.SCREEN_WIDTH * 0.7,
    marginTop:-45,
    alignSelf:'center',
   overflow:'hidden',
    borderRadius:13,
    borderColor:colors.COLOR_PRIMARY,
    borderWidth:0.8
},
bottomContainer:{
    position:'absolute',
    bottom:0,left:0,right:0,
    height:45,
    // backgroundColor:'rgba(30,30,30,0.7)'
    backgroundColor:'rgba(0,0,0,0.91)'
  
  },
  image:{height:18,width:18,marginHorizontal:6},
  labelText:{color:'grey',marginHorizontal:4,fontSize:15},
  valueText:{color:'white',marginHorizontal:4}


  

}
)
