import Snackbar from 'react-native-snackbar';
import { ToastAndroid } from 'react-native';


export const showMessage = async (message,error = true) => {
  Snackbar.show({
    title: message,
    duration: Snackbar.LENGTH_LONG,
    backgroundColor:'black',
    color: error ?  'red' : 'yellow',
   
  });
}


export const showToastMessage = async (message) => {
  ToastAndroid.show(message,ToastAndroid.SHORT)
}




